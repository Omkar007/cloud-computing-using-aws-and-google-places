package edu.syr.ospatil.exploreplaces.app;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.internal.ge;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import com.amazonaws.mobile.auth.core.DefaultSignInResultHandler;
import com.amazonaws.mobile.auth.core.IdentityManager;
import com.amazonaws.mobile.auth.core.IdentityProvider;
import com.amazonaws.mobile.auth.ui.SignInActivity;
import edu.syr.ospatil.exploreplaces.R;
import edu.syr.ospatil.exploreplaces.util.FileCreationHelper;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.services.s3.AmazonS3Client;
import static edu.syr.ospatil.exploreplaces.app.RecyclerviewFragment.mShowingBack;


public class MainActivity extends AppCompatActivity implements LocationListener,RecyclerviewFragment.CustomOnClickListener, GoogleApiClient.OnConnectionFailedListener,
        RecyclerviewFragment2.CustomOnClickListener,Recyclerviewfragment3.CustomOnClickListener,RecyclerviewFragment4.CustomOnClickListener,
        RecyclerviewFragment5.CustomOnClickListener,RecyclerviewFragment6.CustomOnClickListener,RecyclerviewFragment7.CustomOnClickListener{
    int movieno,pageno;
    private NavigationView navigationView;
    static DrawerLayout drawer;
    private View navHeader;
    static  ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;
    static  public Toolbar toolbar,bottomT;
    private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private Uri filePath;
    public static int navItemIndex = 0;
    private static final String TAG_HOME = "Home";
    private static final String TAG_Aboutus = "MY_FRAGMENT";
    private static final String TAG_Task3 = "Task 3";
    public static String CURRENT_TAG = TAG_HOME;
    private String[] activityTitles;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private static final int PICK_IMAGE_REQUEST = 234;
    /** The identity manager used to keep track of the current user account. */
    private IdentityManager identityManager;
    private Handler mHandler;
    static  TextView mTitle;
    Fragment fragment,fragment2;
    public static String mobile,rating;
    public static Bitmap image;
    static String id;
    static Boolean fabhidden=true;
    FloatingActionButton fab=null;
    private GoogleApiClient mGoogleApiClient;
    int PLACE_PICKER_REQUEST = 1;
    ActionBarDrawerToggle actionBarDrawerToggle;
    List<Travel> movieList;
    static String latlong="43.03767,-76.13399";
    private LocationManager locationManager;
    private String provider;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        identityManager = IdentityManager.getDefaultIdentityManager();
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AWSMobileClient.getInstance().initialize(this).execute();
        Explode enterTransition = new Explode();
        enterTransition.setDuration(1000);
        Explode exitTransaction = new Explode();
        exitTransaction.setDuration(1000);
        getWindow().setEnterTransition(enterTransition);
        getWindow().setExitTransition(exitTransaction);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        if(provider !=null){
            provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                System.out.println("Provider " + provider + " has been selected.");
                onLocationChanged(location);
            } else {
                System.out.println("No gps activated");
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        bottomT = (Toolbar) findViewById (R . id . bot_toolbar );
        bottomT . inflateMenu ( R. menu . bottom_toolbar );
        EnhancedMenuInflater.inflate(getMenuInflater(),bottomT.getMenu(), false);
        setupBottomToolbarItemSelected () ;

        getSupportActionBar().setDisplayShowTitleEnabled ( false );

        mTitle = (TextView)toolbar . findViewById (R. id . toolbar_title );

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtWebsite = (TextView) navHeader.findViewById(R.id.website);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        mTitle.setText(activityTitles[navItemIndex]);
        loadNavHeader();
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
       // loadProfileImage();                                       //to be enabled
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });
        fab = (FloatingActionButton) findViewById(R.id.fab);
        final GestureDetector gestureDetector=new GestureDetector(this,new SingleTapDetector());
        fab.setOnTouchListener(new View.OnTouchListener() {
            float dX=0;
            float dY=0;
            int lastAction=-1;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {

                } else {

                    switch (event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            dX = v.getX() - event.getRawX();
                            dY = v.getY() - event.getRawY();
                            lastAction = MotionEvent.ACTION_DOWN;
                            break;

                        case MotionEvent.ACTION_MOVE:
                            v.setY(event.getRawY() + dY);
                            v.setX(event.getRawX() + dX);
                            lastAction = MotionEvent.ACTION_MOVE;
                            break;

                        case MotionEvent.ACTION_UP:
                            if (lastAction == MotionEvent.ACTION_DOWN) {
                                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                                try {
                                    startActivityForResult(builder.build(MainActivity.this), PLACE_PICKER_REQUEST);
                                } catch (GooglePlayServicesRepairableException e) {
                                    e.printStackTrace();
                                } catch (GooglePlayServicesNotAvailableException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;

                        default:
                            return false;
                    }
                }
                return true;
            }
        });

        fab.hide();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
    }

    @Override
    public void onLocationChanged(Location location) {

        double lat = (double) (location.getLatitude());
        double lng = (double) (location.getLongitude());
        latlong = String.valueOf(lat)+","+String.valueOf(lng);
    }


    private class SingleTapDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

            try {
                startActivityForResult(builder.build(MainActivity.this), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e1) {
                e1.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e1) {
                e1.printStackTrace();
            }
            return true;
        }

    }
    private void setupBottomToolbarItemSelected () {
        bottomT . setOnMenuItemClickListener (new Toolbar. OnMenuItemClickListener () {
            @Override
            public boolean onMenuItemClick ( MenuItem item ) {
                int id = item . getItemId () ;
                switch ( id ){
                    case R. id . bottom_action3 :
                        if(fragment instanceof ViewPagerFragment)
                            if(ViewPagerFragment.pageno==0)
                            {
                                Travel.flag = false;
                                Collections.sort(RecyclerviewFragment.movieList);
                                RecyclerviewFragment.mAdapter.notifyDataSetChanged();

                            }
                            else if(ViewPagerFragment.pageno==1)
                            {
                                Travel.flag = false;
                                Collections.sort(RecyclerviewFragment2.movieList);
                                RecyclerviewFragment2.mAdapter.notifyDataSetChanged();
                            }
                            else if(ViewPagerFragment.pageno==2)
                            {
                                Travel.flag = false;
                                Collections.sort(Recyclerviewfragment3.movieList);
                                Recyclerviewfragment3.mAdapter.notifyDataSetChanged();
                            }
                            else if(ViewPagerFragment.pageno==3)
                            {
                                Travel.flag = false;
                                Collections.sort(RecyclerviewFragment4.movieList);
                                RecyclerviewFragment4.mAdapter.notifyDataSetChanged();
                            }
                            else if(ViewPagerFragment.pageno==4)
                            {
                                Travel.flag = false;
                                Collections.sort(RecyclerviewFragment5.movieList);
                                RecyclerviewFragment5.mAdapter.notifyDataSetChanged();
                            }
                            else if(ViewPagerFragment.pageno==5)
                            {
                                Travel.flag = false;
                                Collections.sort(RecyclerviewFragment6.movieList);
                                RecyclerviewFragment6.mAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                Travel.flag = false;
                                Collections.sort(RecyclerviewFragment7.movieList);
                                RecyclerviewFragment7.mAdapter.notifyDataSetChanged();
                            }
                        return true ;
                    case R. id . bottom_action4 :
                        if(fragment instanceof ViewPagerFragment)
                        {
                            if(fabhidden) {
                                fabhidden=false;
                                fab.show();
                            }
                            else
                            {
                                fabhidden=true;
                                fab.hide();
                            }
                        }
                        return true ;
                }
                return false ;
            }
        }) ;
        bottomT . setNavigationIcon (R.drawable.ic_visibility_off_black_24dp);
        bottomT . setNavigationOnClickListener ( new View . OnClickListener () {
            @Override
            public void onClick ( View v ) {
                bottomT . setVisibility ( View . GONE );
            }
        }) ;
    }


    @Override
    public void onClicked(View v,int position,List<Travel> movielist) {
        movieno=position;
        this.movieList=movielist;
        pageno=ViewPagerFragment.pageno;
        FragmentManager fragmentManager = getSupportFragmentManager();
        ViewPagerFragment masterFragment = (ViewPagerFragment) fragmentManager.findFragmentByTag(CURRENT_TAG);
        FragmentManager fm = masterFragment.getFragmentManager();
        TravelFragment detailFragment=(TravelFragment)fm.findFragmentByTag("DetailFragment");
        FragmentTransaction ft = fm.beginTransaction();
        Bundle bundle = new Bundle();
        if (detailFragment == null)
        {
            detailFragment = new TravelFragment();
        }
        fragment2=detailFragment;

        bundle.putSerializable("movieList", (Serializable) movieList);
        bundle.putInt("moviePos", position);
        detailFragment.setArguments(bundle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        ft.replace(R.id.frame, detailFragment, "DetailFragment");
        ft.addToBackStack(null);
        ft.commit();
    }

    private void loadNavHeader() {
        Glide.with(this).load(urlNavHeaderBg)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);
    }

    public void setLogout(){
            // The user is currently signed in with a provider. Sign out of that provider.
            identityManager.signOut();
            identityManager.setUpToAuthenticate(this, new DefaultSignInResultHandler() {

                @Override
                public void onSuccess(Activity activity, IdentityProvider identityProvider) {
                    // User has signed in
                    Log.e("Success", "User signed in");
                    activity.finish();
                }

                @Override
                public boolean onCancel(Activity activity) {
                    return false;
                }
            });
            SignInActivity.startSignInActivity(this, Application.sAuthUIConfiguration);
            return;
    }

    private void loadHomeFragment() {
        selectNavMenu();
        if(navItemIndex == 1 || getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) == null)
        {}
        else {
            mTitle.setText(activityTitles[navItemIndex]);
        }
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();
            return;
        }

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                fragment = getHomeFragment();
                if(fragment!=null) {
                    if(navItemIndex==1 )
                    {}
                    else {
                        mTitle.setText(activityTitles[navItemIndex]);
                    }
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                    //fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commitAllowingStateLoss();
                }
            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        drawer.closeDrawers();
        invalidateOptionsMenu();
    }

    Fragment getHomeFragment() {

        switch (navItemIndex) {
            case 0:
                ViewPagerFragment fragment=new ViewPagerFragment();
                bottomT.setVisibility(View.VISIBLE);
                return fragment;

            default:
                return null;
        }
    }


    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.nav_task3:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_Task3;
                        break;
                    case R.id.nav_aboutus:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_Aboutus;
                        break;
                    case R.id.nav_task4:
                        navItemIndex = 3;
                        setLogout();
                        break;
                    default:
                        navItemIndex = 0;
                }

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);
                loadHomeFragment();

                return true;
            }
        });

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        else if (shouldLoadHomeFragOnBackPress) {
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;

                loadHomeFragment();
                return;
            }
            else if (mShowingBack) {
                getFragmentManager().popBackStack();
                mShowingBack=false;
                return;
            }
            else if(navItemIndex == 0 && (fragment2!=null))
            {
                fragment2=null;
                mTitle.setText(activityTitles[navItemIndex]);
                super.onBackPressed();

            }
            else {
                AlertDialog.Builder b2 = new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Do you really want to close this app?")
                        .setCancelable(true)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog b3 = b2.create();
                b3.setTitle("Exit");
                b3.show();
            }
        }
    }

public void uploadImage(){
    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    startActivityForResult(Intent.createChooser(galleryIntent, "Select Picture"), PICK_IMAGE_REQUEST);
}

    @Override
    protected void onResume() {
        super.onResume();

        if (!IdentityManager.getDefaultIdentityManager().isUserSignedIn()) {
            // In the case that the activity is restarted by the OS after the application
            // is killed we must redirect to the splash activity to handle the sign-in flow.
            Intent intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                id=place.getId();
                FragmentManager fragmentManager = getSupportFragmentManager();
                ViewPagerFragment masterFragment = (ViewPagerFragment) fragmentManager.findFragmentByTag(CURRENT_TAG);
                FragmentManager fm = masterFragment.getFragmentManager();
                TravelFragment detailFragment=(TravelFragment)fm.findFragmentByTag("DetailFragment");
                FragmentTransaction ft = fm.beginTransaction();
                if (detailFragment == null)
                {
                    detailFragment = new TravelFragment();
                }
                fragment2=detailFragment;

                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                ft.replace(R.id.frame, detailFragment, "DetailFragment");
                ft.addToBackStack(null);
                ft.commit();
            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
                if(filePath!=null){
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            uploadWithTransferUtility();
                        }
                    });
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void uploadWithTransferUtility() {
        FileCreationHelper fileHelper = new FileCreationHelper();
        try{
            final String fileName = "omkar07";
            final File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    "/" + fileName);
            fileHelper.createFile(getApplicationContext(), filePath, file);
            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(getApplicationContext())
                            .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                            .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                            .build();

            TransferObserver uploadObserver =
                    transferUtility.upload("public/" + fileName + "." + fileHelper.getFileExtension(getApplicationContext(),filePath), file);

            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        Toast.makeText(getApplicationContext(), "Upload Completed!", Toast.LENGTH_SHORT).show();
                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        imgProfile.setImageBitmap(bitmap);
                        file.delete();
                    } else if (TransferState.FAILED == state) {
                        file.delete();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int)percentDonef;

                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent
                            + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                }
            });
            Log.d("YourActivity", "Bytes Transferrred: " + uploadObserver.getBytesTransferred());
            Log.d("YourActivity", "Bytes Total: " + uploadObserver.getBytesTotal());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void loadProfileImage(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                downloadFile();
            }
        });
    }
    public void downloadFile(){
        final String fileName = "omkar07";
        try{
            final File localFile = File.createTempFile("images",".jpg");
            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(getApplicationContext())
                            .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                            .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                            .build();

            TransferObserver downloadObserver =
                    transferUtility.download("public/"+ fileName + ".jpg",localFile);
            downloadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        Toast.makeText(getApplicationContext(), "Download Completed!", Toast.LENGTH_SHORT).show();
                        Bitmap bmp = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        imgProfile.setImageBitmap(bmp);
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("MainActivity", "   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    ex.printStackTrace();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

}


