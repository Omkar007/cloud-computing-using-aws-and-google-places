package edu.syr.ospatil.exploreplaces.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.syr.ospatil.exploreplaces.R;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.wasabeef.recyclerview.animators.FlipInBottomXAnimator;


public class RecyclerviewFragment6 extends Fragment {
    static List<Travel> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    static TravelAdapter mAdapter;
    static Boolean mShowingBack = false;

    public RecyclerviewFragment6() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public interface CustomOnClickListener {
        public void onClicked(View v, int position, List<Travel> movieList);
    }

    private CustomOnClickListener customOnClickListener;

    public void buttonClicked ( View v,int position , List<Travel> movieList) {
        customOnClickListener . onClicked (v,position,movieList);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        this.customOnClickListener = (MainActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_recyclerview_fragment6, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view6);

        mAdapter = new TravelAdapter(movieList,getContext());
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new android.support.v7.widget.DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(mAdapter);
        ScaleInAnimationAdapter scaleadapter = new ScaleInAnimationAdapter(alphaAdapter);
        scaleadapter.setDuration(700);
        recyclerView.setAdapter(scaleadapter);
        //  recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new TravelAdapter.ClickListener() {

            @Override
            public void onItemClick(int position, View v,List<Travel> movielist) {
                buttonClicked(v, position,movielist);
            }


            @Override
            public void onItemLongClick(int position, View v) {
                getActivity().startActionMode(new ActionBarCallBack(position));

            }

            @Override
            public void onOverflowMenuClick(final int position, View v) {
                PopupMenu popup = new PopupMenu(getActivity(), v);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch (id) {
                            case R.id.action_delete:
                                movieList.remove(position);
                                FadeInAnimator animator = new FadeInAnimator();
                                animator.setInterpolator(new OvershootInterpolator());
                                animator.setRemoveDuration(500);
                                recyclerView.setItemAnimator(animator);
                                mAdapter.notifyItemRemoved(position);

                                mAdapter.notifyItemRangeChanged(0, movieList.size());

                                return true;
                            case R.id.action_duplicate:
                                movieList.add(position + 1, movieList.get(position));
                                FlipInBottomXAnimator animator2 = new FlipInBottomXAnimator();
                                animator2.setInterpolator(new OvershootInterpolator());
                                animator2.setRemoveDuration(1000);
                                recyclerView.setItemAnimator(animator2);
                                mAdapter.notifyItemInserted(position + 1);

                                return true;
                        }
                        return false;
                    }
                });
                MenuInflater menuInflater = popup.getMenuInflater();
                menuInflater.inflate(R.menu.cab, popup.getMenu());
                popup.show();
            }
        });


        if(movieList.isEmpty()) {
            new JSONP().execute(ViewPagerFragment.s6);
        }


        return v;
    }

    void prepareMovieData(Travel movie[]) {
        movieList.clear();
        for (int i = 0; i <movie.length; i++) {

            movieList.add(movie[i]);
        }
        if(mAdapter!=null)
            mAdapter.notifyDataSetChanged();
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    class ActionBarCallBack implements ActionMode.Callback {
        int position;

        public ActionBarCallBack(int position) {
            this.position = position;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.cab, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem
                item) {
            int id = item.getItemId();
            switch (id) {
                case R.id.action_delete:

                    movieList.remove(position);
                    FadeInAnimator animator = new FadeInAnimator();
                    animator.setInterpolator(new OvershootInterpolator());
                    animator.setRemoveDuration(500);
                    recyclerView.setItemAnimator(animator);
                    mAdapter.notifyItemRemoved(position);

                    mAdapter.notifyItemRangeChanged(0, movieList.size());

                    mode.finish();
                    break;
                case R.id.action_duplicate:
                    movieList.add(position + 1, movieList.get(position));
                    FlipInBottomXAnimator animator2 = new FlipInBottomXAnimator();
                    animator2.setInterpolator(new OvershootInterpolator());
                    animator2.setRemoveDuration(1000);
                    recyclerView.setItemAnimator(animator2);
                    mAdapter.notifyItemInserted(position + 1);

                    mode.finish();
                    break;
                default:
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
        }
    }

    public void sortyear() {
        Travel.flag = false;
        Collections.sort(movieList);
        mAdapter.notifyDataSetChanged();


    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    public void flipCard() {
        if (mShowingBack) {
            getActivity().getFragmentManager().popBackStack();
            mShowingBack = false;
            return;
        }

        mShowingBack = true;
        MapsFragment mp=new MapsFragment();
        Bundle args = new Bundle();
        args.putInt("no", ViewPagerFragment.pageno);
        mp.setArguments(args);
        getActivity().getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.card_flip_right_in,
                        R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in,
                        R.animator.card_flip_left_out)

                .replace(R.id.frame, mp)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (menu.findItem(R.id.action_search) == null) {

            inflater.inflate(R.menu.search2, menu);

        }

        SearchView search = (SearchView)
                menu.findItem(R.id.action_search).getActionView();

        if (search != null) {

            search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    int pos = -1;

                    for (int i = 0; i < movieList.size(); i++) {
                        if (org.apache.commons.lang3.StringUtils.containsIgnoreCase((String) movieList.get(i).getTitle(), query))
                            pos = i;
                    }


                    if (pos >= 0)
                        recyclerView.scrollToPosition(pos);

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    return true;
                }
            });
        }

        MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        };

        MenuItem actionMenuItem = menu.findItem(R.id.action_search);

        MenuItemCompat.setOnActionExpandListener(actionMenuItem, expandListener);
    }

    void setJSON(String s)
    {
        new JSONP().execute(s);
    }

    class JSONP extends AsyncTask<String,Void,String>
    {
        String jsonurl,jsonstring;
        RecyclerviewFragment fragment;
        Travel movie[];

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            jsonurl=params[0]+"&location="+MainActivity.latlong;

            String data="";
            try {
                URL url=new URL(jsonurl);
                HttpURLConnection httpURLConnection= (HttpURLConnection) url.openConnection();

                httpURLConnection.setRequestMethod("GET");


                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder  stringBuilder=new StringBuilder();
                while((jsonstring= bufferedReader.readLine())!=null)
                {
                    stringBuilder.append(jsonstring+"\n");
                }

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();

                JSONObject jsonRootObject = new JSONObject(stringBuilder.toString());
                JSONArray jsonArray = jsonRootObject.optJSONArray("results");
                movie = new Travel[jsonArray.length()];
                String image="www.example.com";
                String opennow="true";
                for(int i=0; i < jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String pricing = jsonObject.optString("price_level").toString();
                    String title= jsonObject.optString("name").toString();
                    String rating = jsonObject.optString("rating").toString();
                    String id=jsonObject.optString("place_id").toString();
                    JSONObject openinghours = jsonObject.optJSONObject("opening_hours");
                    if(openinghours!=null) {
                        opennow = String.valueOf(openinghours.optBoolean("open_now"));
                    }

                    JSONArray jsonArray2 = jsonObject.optJSONArray("photos");
                    if(jsonArray2!=null) {
                        JSONObject jsonObject2 = jsonArray2.getJSONObject(0);
                        image = jsonObject2.optString("photo_reference").toString();
                    }
                    movie[i]=new Travel(title,image,id,rating,pricing,opennow);
                }
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  data;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            prepareMovieData(movie);

        }
    }
}





