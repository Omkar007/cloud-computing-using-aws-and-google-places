package edu.syr.ospatil.exploreplaces.app;


import edu.syr.ospatil.exploreplaces.POJO.Example;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;


public interface RetrofitMaps {

    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
     */
    @GET("api/place/nearbysearch/json?sensor=true&key=AIzaSyCM5EeoVLc22gV-FTnsu9iaKVWbLfUrScc")
    Call<Example> getNearbyPlaces(@Query("types") String type, @Query("location") String location, @Query("radius") int radius);

}