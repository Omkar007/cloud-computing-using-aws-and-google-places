package edu.syr.ospatil.exploreplaces.app;

import edu.syr.ospatil.exploreplaces.POJO2.Example;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;


public interface RetrofitMaps2 {
    @GET("api/directions/json?key=AIzaSyCM5EeoVLc22gV-FTnsu9iaKVWbLfUrScc")
    Call<Example> getDistanceDuration(@Query("units") String units, @Query("origin") String origin,
                                      @Query("destination") String destination, @Query("mode") String mode);

}
